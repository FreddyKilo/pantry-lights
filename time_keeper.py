from utime import time
import urequests as requests
import json


class TimeKeeper:

    def __init__(self):
        self.last_request = 0
        self.is_sun_up = False

    def is_sunset(self):
        try:
            # make a request to get sunset info every 300 sec (5 min)
            if self.last_request == 0 or time() > self.last_request + 300:
                self.last_request = time()
                resp = requests.get(URL.SUNSET_TIME)
                body = json.loads(resp.text)
                self.is_sun_up = body['isSunUp']
            return not self.is_sun_up

        except Exception as e:
            print('Error in is_sunset(), is server running?')
            print(repr(e))
            return False


class URL:
    __BASE_URL = 'http://192.168.0.19:5000'

    SUNSET_TIME = __BASE_URL + '/sunset'
    CURRENT_DATE = __BASE_URL + '/current-date'
