import wifi
from decorators import *
from output_interface import *
from machine import Pin, PWM
from pinout import Wemos
from time_keeper import TimeKeeper
from utime import sleep_ms, sleep_us

led = LED_BUILTIN()

# Set up pin D1 as PWM for lights
lights = PWM(Pin(Wemos.D1, Pin.OUT), freq=120)
# Set up IR sensor input on pin D2
ir_pin = Pin(Wemos.D2, Pin.IN)

lights_on = False
night_light_status = False
FADE_VALUE = 64
PWM_RANGE = 1024
fade_in_start_index = 0

time_keeper = TimeKeeper()


def ready():
    # led.start_up()  # Give time to manually open up a serial monitor
    print_info('System platform: ' + sys.platform)


def set():
    if wifi.connect():
        led.heart_beat()
        led.heart_beat()
        print_info('Connected to WiFi!')
        return

    print_error("Could not connect to WiFi\n\tAre you using the correct SSID and password?")
    led.show_error(Error.WIFI_NOT_CONNECTED)


@loop
def go():
    global fade_in_start_index
    global lights_on
    global night_light_status

    if time_keeper.is_sunset():
        if motion_detected() and not lights_on:
            fade_in(fade_in_start_index, PWM_RANGE)
            print('Motion detected and lights fully illuminated')
            lights_on = True

        elif not motion_detected() and lights_on:
            fade_in_start_index = fade_out(PWM_RANGE, FADE_VALUE)
            print('No motion detected and lights faded to nightlight status')
            lights_on = False

        elif not motion_detected() and not night_light_status:
            fade_in(fade_in_start_index, FADE_VALUE)
            print('Lights have been initially faded in to nightlight status')
            fade_in_start_index = FADE_VALUE

        night_light_status = True

    else:
        if motion_detected() and not lights_on:
            fade_in(fade_in_start_index, PWM_RANGE)
            print('Motion detected and lights fully illuminated')
            lights_on = True

        elif not motion_detected() and lights_on:
            fade_in_start_index = fade_out(PWM_RANGE, 0)
            print('No motion detected and lights turned completely off')
            lights_on = False

        elif not motion_detected() and night_light_status:
            fade_out(FADE_VALUE, 0)
            print('Lights have been initially turned completely off')

        night_light_status = False

    sleep_ms(50)


def motion_detected():
    return ir_pin.value() == 1


def fade_in(low, high):
    for i in range(low, high):
        lights.duty(i)
        sleep_us(100)


def fade_out(high, low):
    for i in range(high - low):
        lights.duty(high - i - 1)
        sleep_ms(5)
        # Return the index to pass into fade_in() if fade_out()
        # gets interrupted by a high input from the ir sensor
        if ir_pin.value() == 1:
            return high - i

    return low
