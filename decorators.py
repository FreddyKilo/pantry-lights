def loop(func):
    def looper():
        while True:
            func()

    return looper
