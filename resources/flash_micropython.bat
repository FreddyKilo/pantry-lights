::If using a NODEMCU there is no need to put it into boot loader mode, this script does that

@echo off

esptool.py --port COM4 erase_flash

esptool.py --port COM4 --baud 115200 write_flash --flash_size=detect -fm dio 0 esp8266-20180511-v1.9.4.bin
echo Firmware flashed with MicroPython!
echo;
