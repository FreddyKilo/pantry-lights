class Wemos:
    D0 = 16
    D1 = 5
    D2 = 4
    D3 = 0
    D4 = 2
    D5 = 14
    D6 = 12
    D7 = 13
    D8 = 15
    RX = 3
    TX = 1
    ESP_LED = D4


class NodeMCU:
    D0 = 16
    D4 = 2
    NODE_LED = D0
    ESP_LED = D4
